# -*- coding: utf-8 -*-

# Copyright (c) 2014 CoNWeT Lab., Universidad Politécnica de Madrid
# Copyright (c) 2018 Future Internet Consulting and Development Solutions S.L.

# This file is part of OAuth2 CKAN Extension.

# OAuth2 CKAN Extension is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# OAuth2 CKAN Extension is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with OAuth2 CKAN Extension. If not, see <http://www.gnu.org/licenses/>.


import base64
import ckan.model as model
from ckanext.oauth2 import db
import json
import logging
from six.moves.urllib.parse import urljoin
import os

from base64 import b64encode, b64decode
from ckan.plugins import toolkit
import ckan.authz as authz
from oauthlib.oauth2 import InsecureTransportError
import requests
from requests_oauthlib import OAuth2Session
import six

import jwt

from ckanext.oauth2 import constants
from flask import jsonify

log = logging.getLogger(__name__)


def generate_state(url):
    return b64encode(str.encode(json.dumps({constants.CAME_FROM_FIELD: url})))


def get_came_from(state):
    return json.loads(b64decode(state)).get(constants.CAME_FROM_FIELD, '/')


REQUIRED_CONF = (
    "authorization_endpoint", "token_endpoint", "client_id",
    "client_secret", "profile_api_url", "profile_api_user_field",
    "profile_api_mail_field"
    )


class OAuth2Helper(object):

    def __init__(self):

        self.verify_https = \
             os.environ.get('OAUTHLIB_INSECURE_TRANSPORT', '') == ""
        if self.verify_https and \
                os.environ.get("REQUESTS_CA_BUNDLE", "").strip() != "":
            self.verify_https = os.environ["REQUESTS_CA_BUNDLE"].strip()

        self.jwt_enable = six.text_type(os.environ.get(
            'CKAN_OAUTH2_JWT_ENABLE', toolkit.config.get(
                'ckan.oauth2.jwt.enable', ''
                )
            )).strip().lower() in ("true", "1", "on")

        self.legacy_idm = six.text_type(os.environ.get(
            'CKAN_OAUTH2_LEGACY_IDM', toolkit.config.get(
                'ckan.oauth2.legacy_idm', ''
                )
            )).strip().lower() in ("true", "1", "on")
        self.authorization_endpoint = six.text_type(os.environ.get(
            'CKAN_OAUTH2_AUTHORIZATION_ENDPOINT', toolkit.config.get(
                'ckan.oauth2.authorization_endpoint', ''
                )
            )).strip()
        self.token_endpoint = six.text_type(os.environ.get(
            'CKAN_OAUTH2_TOKEN_ENDPOINT', toolkit.config.get(
                'ckan.oauth2.token_endpoint', ''
                )
            )).strip()
        self.profile_api_url = six.text_type(os.environ.get(
            'CKAN_OAUTH2_PROFILE_API_URL', toolkit.config.get(
                'ckan.oauth2.profile_api_url', ''
                )
            )).strip()
        self.client_id = six.text_type(os.environ.get(
            'CKAN_OAUTH2_CLIENT_ID', toolkit.config.get(
                'ckan.oauth2.client_id', ''
                )
            )).strip()
        self.client_secret = six.text_type(os.environ.get(
            'CKAN_OAUTH2_CLIENT_SECRET', toolkit.config.get(
                'ckan.oauth2.client_secret', ''
                )
            )).strip()
        self.scope = six.text_type(os.environ.get(
            'CKAN_OAUTH2_SCOPE', toolkit.config.get('ckan.oauth2.scope', '')
            )).strip()
        self.rememberer_name = six.text_type(os.environ.get(
            'CKAN_OAUTH2_REMEMBER_NAME', toolkit.config.get(
                'ckan.oauth2.rememberer_name', 'auth_tkt'
                )
            )).strip()
        self.profile_api_user_field = six.text_type(os.environ.get(
            'CKAN_OAUTH2_PROFILE_API_USER_FIELD', toolkit.config.get(
                'ckan.oauth2.profile_api_user_field', ''
                )
            )).strip()
        self.profile_api_fullname_field = six.text_type(os.environ.get(
            'CKAN_OAUTH2_PROFILE_API_FULLNAME_FIELD', toolkit.config.get(
                'ckan.oauth2.profile_api_fullname_field', ''
                )
            )).strip()
        self.profile_api_mail_field = six.text_type(os.environ.get(
            'CKAN_OAUTH2_PROFILE_API_MAIL_FIELD', toolkit.config.get(
                'ckan.oauth2.profile_api_mail_field', ''
                )
            )).strip()
        self.profile_api_orgmembership_field = six.text_type(os.environ.get(
            'CKAN_OAUTH2_PROFILE_API_ORGMEMBERSHIP_FIELD', toolkit.config.get(
                'ckan.oauth2.profile_api_orgmembership_field', ''
                )
            )).strip()
        self.organization_name_for_group_role = six.text_type(
            os.environ.get(
                'CKAN_OAUTH2_ORGANIZATION_NAME_FOR_GROUP_ROLE', toolkit.config.get(
                    'ckan.oauth2.organization_name_for_group_role', ''
                )
            )).strip()
        self.sysadmin_group_name = six.text_type(os.environ.get(
            'CKAN_OAUTH2_SYSADMIN_GROUP_NAME', toolkit.config.get(
                'ckan.oauth2.sysadmin_group_name', ''
                )
            )).strip()

        self.redirect_uri = urljoin(urljoin(toolkit.config.get(
            'ckan.site_url', 'http://localhost:5000'
            ), toolkit.config.get('ckan.root_path')), constants.REDIRECT_URL)

        # Init db
        db.init_db(model)

        missing = [key for key in REQUIRED_CONF if getattr(
            self, key, ""
            ) == ""]
        if missing:
            raise ValueError(
                "Missing required oauth2 conf: %s" % ", ".join(missing)
                )
        elif self.scope == "":
            self.scope = None

    def challenge(self, came_from_url):
        # This function is called by the log in function when
        # the user is not logged in
        state = generate_state(came_from_url)
        oauth = OAuth2Session(
            self.client_id, redirect_uri=self.redirect_uri,
            scope=self.scope, state=state
            )
        auth_url, _ = oauth.authorization_url(self.authorization_endpoint)
        log.debug(
            'Challenge: Redirecting challenge to page {0}'.format(auth_url)
            )
        # CKAN 2.6 only supports bytes
        return toolkit.redirect_to(auth_url)

    def get_token(self):
        oauth = OAuth2Session(
            self.client_id, redirect_uri=self.redirect_uri, scope=self.scope
            )

        # Just because of FIWARE Authentication
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded',
        }

        if self.legacy_idm:
            # This is only required for Keyrock v6 and v5
            headers['Authorization'] = 'Basic %s' % base64.urlsafe_b64encode(
                '%s:%s' % (self.client_id, self.client_secret)
            )

        try:
            token = oauth.fetch_token(
                self.token_endpoint,
                headers=headers,
                client_secret=self.client_secret,
                client_id=self.client_id,
                authorization_response=toolkit.request.url.replace('http:', 'https:', 1),
                verify=self.verify_https
                )
        except requests.exceptions.SSLError as e:
            # TODO search a better way to detect invalid certificates
            if "verify failed" in six.text_type(e):
                raise InsecureTransportError()
            else:
                raise

        return token

    def identify(self, token):
        if self.jwt_enable:
            log.info('jwt_enabled')
            access_token = bytes(token['access_token'])
            user_data = jwt.decode(access_token, verify=False)
            user = self.user_json(user_data)

        else:
            try:
                if self.legacy_idm:
                    profile_response = requests.get(self.profile_api_url + '?access_token=%s' % token['access_token'], verify=self.verify_https)
                else:
                    oauth = OAuth2Session(self.client_id, token=token)
                    profile_response = oauth.get(self.profile_api_url)

            except requests.exceptions.SSLError as e:
                log.debug('exception identify oauth2')
                # TODO search a better way to detect invalid certificates
                if "verify failed" in six.text_type(e):
                    raise InsecureTransportError()
                else:
                    raise

            # Token can be invalid
            if not profile_response.ok:
                error = profile_response.json()
                if error.get('error', '') == 'invalid_token':
                    raise ValueError(error.get('error_description'))
                else:
                    profile_response.raise_for_status()
            else:
                user_data = profile_response.json()
                user = self.user_json(user_data)

        # Save the user in the database
        model.Session.add(user)
        model.Session.commit()
        model.Session.remove()

        changedGroups = False
        if self.profile_api_orgmembership_field and self.profile_api_orgmembership_field in user_data:
            membership = model.Session.query(model.Member).filter(model.Member.table_name == 'user').filter(model.Member.table_id == user.id).all()
            for org in user_data[self.profile_api_orgmembership_field]:
                # expect organization to be {org: '<org-name>', role: '<role>' }
                if isinstance(org, dict):
                    org_name = org['org']
                    # Adjust the name of the organization so it can be used as part of an url,
                    # e. g. "Tegel Projekt GmbH" to "tegel-projekt-gmbh"
                    org_name_url = org_name.lower().replace(' ', '-')

                    capacity = org['role'].lower()
                    if not capacity in ["admin", "editor", "member"]:
                        capacity = "member"

                    # There are only two roles(capacities) for groups: member and admin.
                    # Therefore the role editor will be mapped to member or admin.
                    group_capacity = capacity
                    if group_capacity == "editor":
                        group_capacity = "admin"

                    dbOrg = model.Session.query(model.Group).filter(model.Group.name == org_name_url).first()

                    # create Organization if not exist
                    if dbOrg is None:
                        # changedGroups = True
                        dbOrg = model.Group(name = org_name_url, title = org_name, description = org_name)
                        dbOrg.is_organization = True
                        dbOrg.type = 'organization'
                        model.Session.add(dbOrg)
                        model.Session.commit()
                        log.info('Creating the organization %s with id %s', dbOrg.name, dbOrg.id)

                    memberDb = None
                    # check if the user is already added to the group with the desired capacity
                    for memberOf in membership:
                        if memberOf.group_id == dbOrg.id and memberOf.capacity == capacity and memberOf.state == 'active':
                            log.debug(f'User is already member of the Organization: {dbOrg.name} with capacity: {capacity}')
                            memberDb = memberOf
                            break

                    # remove the user from the group in case the user is not member of the group or the capacity does not fit to 
                    # the desired capacity
                    if not memberDb is None:
                        # this db entry has already the right membership: user is added to the organization with the desired capacity
                        # in membership we collect the entries which are not longer matching the desired roles, and therefore have to be deleted from the DB
                        # so we need to remove this entry
                        membership.remove(memberDb)

                    # add the user to the Organization
                    if memberDb is None:
                        member = model.Member(table_name='user', table_id=user.id, capacity=capacity, group=dbOrg)
                        log.info('Add user %s into organization %s with role %s', user.name, dbOrg.name, capacity)
                        # revisions were removed from CKAN 2.9 so the next 2 lines of code had to be removed
                        # rev = model.repo.new_revision()
                        # rev.author = user.id
                        model.Session.add(member)
                        # set mark to update the Session
                        changedGroups = True

                    # get the role from the Organisation "organization_name_for_group_role"
                    if self.organization_name_for_group_role and self.organization_name_for_group_role == org_name:
                        # get all groups which are not organizations
                        dbGroups = model.Session.query(model.Group).filter(model.Group.is_organization == False).all()
                        for dbGroup in dbGroups:
                            memberGroupsDb = None
                            for memberGroupOf in membership:
                                # check if user is already in group with the desired capacity
                                current_group_capacity = authz.users_role_for_group_or_org(dbGroup.id, user.name)

                                if memberGroupOf.group_id == dbGroup.id and current_group_capacity == group_capacity and memberOf.state == 'active':
                                    log.info(f'User is already member of the Group: {dbGroup.name} with capacity: {group_capacity}')
                                    memberGroupsDb = memberGroupOf
                                    break

                            # remove the user from the group in case the user is not member of the group or the capacity does not fit to 
                            # the desired capacity
                            if not memberGroupsDb is None:
                                # this db entry has already the right membership: user is added to the group with the desired capacity
                                # in membership we collect the entries which are not longer matching the desired roles, and therefore have to be deleted from the DB
                                # so we need to remove this entry
                                membership.remove(memberGroupsDb)
                        
                            # add the user to the group
                            if memberGroupsDb is None:
                                groupMember = model.Member(table_name='user', table_id=user.id, capacity=group_capacity, group=dbGroup)
                                log.info('Add user %s into group %s with role %s', user.name, dbGroup.name, group_capacity)
                                model.Session.add(groupMember)
                                changedGroups = True

            # remove all old entries from DB which does not match the desired permissions
            for memberRec in membership:
                changedGroups = True
                log.info('Removing user %s from group %s', user.name, memberRec.group_id)
                model.Session.delete(memberRec)

            # save the changes
            if changedGroups:
                model.Session.commit()
                model.Session.remove()

        return user.name

    def user_json(self, user_data):
        email = user_data[self.profile_api_mail_field]
        user_name = user_data[self.profile_api_user_field]

        # In CKAN can exists more than one user associated with the same email
        # Some providers only allows one account per email
        user = None
        users = model.User.by_email(email)
        if len(users) == 1:
            user = users[0]

        # If the user does not exist, we have to create it...
        if user is None:
            user = model.User(email=email)

        # Now we update their user_name with the one provided
        #  by the OAuth2 service
        # In the future, users will be obtained based on this field
        user.name = user_name

        # Update fullname
        if self.profile_api_fullname_field != "" and \
                self.profile_api_fullname_field in user_data:
            user.fullname = user_data[self.profile_api_fullname_field]

        # Update sysadmin status
        if self.profile_api_orgmembership_field != "" and \
                self.profile_api_orgmembership_field in user_data:
            user.sysadmin = self.sysadmin_group_name in user_data[
                self.profile_api_orgmembership_field
                ]

        return user

    def _get_rememberer(self, environ):
        plugins = environ.get('repoze.who.plugins', {})
        return plugins.get(self.rememberer_name)

    def remember(self, user_name):
        '''
        Remember the authenticated identity.

        This method simply delegates to another identifier
        plugin if configured.
        '''
        log.debug('Repoze OAuth remember')
        environ = toolkit.request.environ
        rememberer = self._get_rememberer(environ)
        identity = {'repoze.who.userid': user_name}
        headers = rememberer.remember(environ, identity)
        response = jsonify()
        for header, value in headers:
            response.headers[header] = value
        return response

    def redirect_from_callback(self, resp_remember):
        '''Redirect to the callback URL after a successful authentication.'''
        state = toolkit.request.params.get('state')
        came_from = get_came_from(state)
        response = jsonify()
        response.status_code = 302
        for header, value in resp_remember.headers:
            response.headers[header] = value
        response.headers['location'] = came_from
        response.autocorrect_location_header = False
        return response

    def get_stored_token(self, user_name):
        user_token = db.UserToken.by_user_name(user_name=user_name)
        if user_token:
            return {
                'access_token': user_token.access_token,
                'refresh_token': user_token.refresh_token,
                'expires_in': user_token.expires_in,
                'token_type': user_token.token_type
            }

    def update_token(self, user_name, token):
        user_token = db.UserToken.by_user_name(user_name=user_name)
        # Create the user if it does not exist
        if not user_token:
            user_token = db.UserToken()
            user_token.user_name = user_name
        # Save the new token
        user_token.access_token = token['access_token']
        user_token.token_type = token['token_type']
        user_token.refresh_token = token.get('refresh_token')
        if 'expires_in' in token:
            user_token.expires_in = token['expires_in']
        else:
            access_token = jwt.decode(user_token.access_token, verify=False)
            user_token.expires_in = access_token['exp'] - access_token['iat']

        model.Session.add(user_token)
        model.Session.commit()

    def refresh_token(self, user_name):
        token = self.get_stored_token(user_name)
        if token:
            client = OAuth2Session(
                self.client_id, token=token, scope=self.scope
                )
            try:
                token = client.refresh_token(
                    self.token_endpoint, client_secret=self.client_secret,
                    client_id=self.client_id, verify=self.verify_https
                    )
            except requests.exceptions.SSLError as e:
                # TODO search a better way to detect invalid certificates
                if "verify failed" in six.text_type(e):
                    raise InsecureTransportError()
                else:
                    raise
            self.update_token(user_name, token)
            log.info('Token for user %s has been updated properly' % user_name)
            return token
        else:
            log.warn('User %s has no refresh token' % user_name)
